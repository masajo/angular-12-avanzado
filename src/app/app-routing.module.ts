import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
// Estrategias de Precargar
import { OptInPreloadingStrategy } from './preload-strategies/opt-in-preloading-strategy';
import { NetworkAwarePreloadingStrategy } from './preload-strategies/networkaware-preloading-strategy';
import { OnDemandPreloadingStrategy } from './preload-strategies/on-demand-preloading-strategy';

// Carga perezosa por defecto de las rutas definidas en los módulos de esta aplicación
const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'home'
  },
  {
    // Cargamos las rutas home/... que estarán definidas en el módulo Home-Routing.module.ts
    path: 'home',
    loadChildren: () => import('./modules/home/home.module').then(m => m.HomeModule),
    data: {
      preload: true // Al estar a TRUE, se va precargar al iniciar el módulo app.module.ts
    }
  },
  {
    // Cargamos las rutas profile/... que estarán definidas en el módulo Profile-Routing.module.ts
    path: 'profile',
    loadChildren: () => import('./modules/profile/profile.module').then(m => m.ProfileModule),
    data: {
      preload: false // Al estar FALSE, no se va a precargar. Su carga será LAZY
    }
  },
  {
    // Cargamos las rutas settings/... que estarán definidas en el módulo Settings-Routing.module.ts
    path: 'settings',
    loadChildren: () => import('./modules/settings/settings.module').then(m => m.SettingsModule),
    data: {
      preload: true // Al estar a TRUE, se va precargar al iniciar el módulo app.module.ts
    }
  },
  {
    path: 'change-detection',
    loadChildren: () => import('./modules/change-detection/change-detection.module').then(m => m.ChangeDetectionModule),
    data: {
      preload: true // Al estar a TRUE, se va precargar al iniciar el módulo app.module.ts
    }
  },
  {
    path: 'directivas',
    loadChildren: () => import('./modules/directivas/directivas.module').then(m => m.DirectivasModule),
    data: {
      preload: true // Al estar a TRUE, se va precargar al iniciar el módulo app.module.ts
    }
  }
  // Aquí es donde se pone el 404
];

@NgModule({
  imports: [RouterModule.forRoot(routes,
    // * ESTRATEGIAS DE PRECARGA (solo podemos poner una)
    {
      // * TODO: Descomentar para ver cómo PRECARGAR TODOS OS MÓDULOS de rutas AL INICIAR LA APLICACIÓN
      //   preloadingStrategy: PreloadAllModules
      // * TODO: Descomentar para ver cómo PRECARGAR SOLO LAS RUTAS QUE TENGAN DATA.PRELOAD A TRUE (ESTRATEGIA OPT-IN)
      // preloadingStrategy: OptInPreloadingStrategy
      // * TODO: Descomentar para ver cómo PRECARGAR SOLO SI LA CONEXIÓN DE NAVEGADOR ES BUENA
      // preloadingStrategy: NetworkAwarePreloadingStrategy
      // * PRECARGAR A TRAVÉS DE ACCIONES EN BOTONES DEL COMPONENTE NAV Y CON ESTATREGIA PERSONALIZADA BAJO DEMANDA
      preloadingStrategy: OnDemandPreloadingStrategy
    }
  )],
  exports: [RouterModule]
})
export class AppRoutingModule { }
