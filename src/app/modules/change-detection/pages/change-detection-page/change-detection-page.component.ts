import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-change-detection-page',
  templateUrl: './change-detection-page.component.html',
  styleUrls: ['./change-detection-page.component.scss']
})
export class ChangeDetectionPageComponent implements OnInit {

  // Variable para controlar si componente Reattach muestra todos los cambios del precio del bitcoin
  live: boolean = true;

  // Lista de items que vamos a pasarle al componente AsyncPipeComponent a través del Observable
  items = [{}]
  // Observable que vaa emitir datos a la lista que va a estar escuchando AsyncPipeComponent
  // El Observable es de tipo BehaviourSubject que es un Subject con un valor inicial
  // Por lo que puede emitir nuevos valores aquien esté suscrito
  // Definimos el valor inicial del Behaviour Subject con la lista vacía
  // Esto quiere decir que el componente que esté suscrito a este BehaviourSubject recibe una lista vacía
  // Nada más suscribirse y luego a través de .next() el behaviour subject va a emitir nuevos valores
  items$ = new BehaviorSubject(this.items)

  constructor() { }

  ngOnInit(): void {
  }

  // Método que sirva para añadir un Item aleatorio entre 1 y 100 a la lista
  // y después, emitirlo al componente
  // que esté suscrito al behaviourSubject
  addItem(){

    const nuevoItem = Math.floor(Math.random() * 100) + 1;

    // En la lista tendremos objetos {numero: UnValor}
    this.items.push(
      {
        numero: nuevoItem
      }
    );

    // Despachamos la lista actualizada al componente suscrito
    // A través de next() le pasamos un nuevo valor a todo aquel suscrito
    this.items$.next(this.items);

  }


}
