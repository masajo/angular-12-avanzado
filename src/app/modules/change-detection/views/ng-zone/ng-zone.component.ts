import { Component, OnInit, NgZone } from '@angular/core';

@Component({
  selector: 'app-ng-zone',
  templateUrl: './ng-zone.component.html',
  styleUrls: ['./ng-zone.component.scss']
})
export class NgZoneComponent implements OnInit {

  // Nivel de progreso: 0% - 100%
  progreso = 0;
  texto:string = '';

  constructor(private _ngZone: NgZone) { }

  ngOnInit(): void {
  }



  // Función de incrementar el progreso en 1
  // Vamos a ir de 0 a 100 y cuando llegue a 100
  // Ejecutaremos el CALLBACK (función) que nos va a venir
  // por parámetro
  incrementarProgreso( terminar: () => void ) {
    // incrementar el progreso en 1 unidad
    this.progreso += 1;
    console.log(`Progreso actual: ${this.progreso}%`);

    if (this.progreso < 100) {
      // Vamos a poner un TIMEOUT para que llegue a 100%
      // Un timeout de 10 milisegundos.
      window.setTimeout(() => {
        // Recursividad: Mientras que no termine el timeOut especificado
        // Sigue incrementando el progreso hasta intentar llegar a 100%
        // para ello se llama a sí misma pasándole la función terminar como parámetro
        // para que pueda seguir incrementando el valor de progreso
        this.incrementarProgreso(terminar)
      }, 10)

    } else {
      // Si llega aquí es que ha llegado a 100% el valor de progreso
      // Es entonces cuando ejecutamos el callback llamado terminar()
      // que recibimos por parámetro
      terminar();
    }
  }


  // ? MÉTODOS QUE REALIZAN LA OPERACIÓN DE INCREMENTO DENTRO/FUERA DEL ANGULAR ZONE

  // * MÉTODO QUE AUMENTA PROGRESO DENTRO DE ANGULAR ZONE
  aumentarDentroNgZone() {
    this.texto = 'DENTRO';
    // lo volvemos a inicializar a 0
    this.progreso = 0;
    this.incrementarProgreso(() => {
      console.log('(DENTRO DE NGZONE) Aumento de progreso realizado')
    });
  }


  // * MÉTODO QUE AUMENTA PROGRESO FUERA DE ANGULAR ZONE Y CUANDO TERMINADO
  // * VUELVE A INCORPORARLO AL ANGULAR ZONE PARA QUE EL HTML MUESTRE EL VALOR FINAL
  aumentarFueraNgZone() {
    this.texto = 'FUERA';
    // lo volvemos a inicializar a 0
    this.progreso = 0;

    // Ejecutamos fuera del ngZone el incremento del progreso
    this._ngZone.runOutsideAngular(() => {
      // Lo que hay aquí dentro el lo que tiene que ejecutar
      // fuera del NGZone, es como hacer un DETACH
      this.incrementarProgreso(() => {

        // Cuando haya acabado de incrementar el progreso
        // volvemos a ejecutar dentro den NgZONE, es como hacer un REATTACH
        this._ngZone.run(() => {
          // Lo que se ejecuta aquí, vuelve a estar en el ngZone
          console.log('(FUERA DE NGZONE) Aumento de progreso realizado');
        });

      });

    });



  }


}
