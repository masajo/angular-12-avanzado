import { Component, OnInit, Injectable, ChangeDetectorRef } from '@angular/core';
import * as Mock from 'mockjs';

// Clase provider de Nombres Aletorios de usuarios con mockjs
@Injectable({
  providedIn: 'root'
})
export class DataListProvider {

  get data() {
    const RandomName = Mock.Random;
    // Devolvemos una lista de nombres aleatorios
    return [
      RandomName.first(),
      RandomName.first(),
      RandomName.first(),
      RandomName.cfirst(),
    ];
  }
}

// Componente de ejemplo de uso de OnDetach
@Component({
  selector: 'app-on-detach',
  templateUrl: './on-detach.component.html',
  styleUrls: ['./on-detach.component.scss']
})
export class OnDetachComponent implements OnInit {

  constructor(private _ref: ChangeDetectorRef, public dataListProvider: DataListProvider) { }

  ngOnInit(): void {

    // Vamos a FORZAR que los cambios en la lista de nombres aleatorios que
    // se van a estar generando constantemente, no se tengan en cuenta
    // en el HTML.
    // Dado que tenemos el ChangeDetectionStrategy a DEFAULT (Al omitirlo de los metados de @Component)
    // Necesitamos hacer un DETACH para que el HTML no actualice los cambios del valor a representar

    // * El DETACH, desacopla el TS del HTML y evita que los cambios en el TS se repliquen en el HTML
    this._ref.detach();

    // CUANDO UN COMPONENTE ESTÁ "DESACOPLADO" tenemos SOLO DOS FORMAS DE DECIRLE A ANGULAR QUE
    // QUE REPLIQUE LOS CAMBIOS EN EL HTML

    // * 1. detectChanges() --> Detecta los cambios en ese momento y los manda al HTML
    // Es decir, manualmente, te decimos cuándo deben el HTML atualizarse

    // * 2. reattach() --> (mostrado en otro ejemplo de este módulo) sirve para volver a ACOPLAR el TS con HTML
    // En el momento que ejecutamos esto, todos los cambios de TS pasan al HTML. Devolver al modo DEFAULT

    // Vamos a hacer que los nombres aletorios se muestren solo cada 3 segundos
    // Así evitamos colapsar el HTML con cambios constantes en la lista de nombres aleatorios
    setInterval(() => {
      this._ref.detectChanges(); // acople momentáneo para paasar al html cada 3 segundos
    }, 3000)


  }

}
