import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnDetachComponent } from './on-detach.component';

describe('OnDetachComponent', () => {
  let component: OnDetachComponent;
  let fixture: ComponentFixture<OnDetachComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OnDetachComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OnDetachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
