import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-on-push',
  templateUrl: './on-push.component.html',
  styleUrls: ['./on-push.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OnPushComponent implements OnInit {

  // Valor que se va a incrementar cada segundo
  // Si usamos la estrategia onPush, solo se pinta una vez
  // concretamente, se pinta elprimer valor que toma
  // Los siguientes cambios que sufra la variable, no se van a
  // mostrar en el HTML
  nTicks = 0;

  constructor() { }

  ngOnInit(): void {
    setInterval(() => {
      // Incrementamos cada segundo el valor de los ticks
      this.nTicks++;
      console.log(`Número de Ticks: ${this.nTicks}`);
    }, 1000);
  }

}
