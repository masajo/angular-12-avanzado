import { ChangeDetectorRef, Component, Injectable, OnInit } from '@angular/core';

// Clase provider de datos que nos a proporcionar números aleatorios
// cada medio segundo
@Injectable({
  providedIn: 'root'
})
export class PrecioBitcoinProvider {

  precio = 100;

  constructor() {
    // Intervalo que nos a dar un precio cada medio segundo
    // El precio aleatorio estará entre 100 y 1000€
    setInterval(() => {
      this.precio = Math.floor(Math.random() * 1000) + 100
      console.log(`Nuevo Precio: ${this.precio}`)
    }, 500);

  }

}




// Componente que va a recibir un @Input desde ChangeDetectionPage llamado enVivo
// para que muestre los valores en vivo o lo deje congelado con un valor concreto
@Component({
  selector: 'app-on-reattach',
  templateUrl: './on-reattach.component.html',
  styleUrls: ['./on-reattach.component.scss'],
  inputs: ['enVivo']
})
export class OnReattachComponent implements OnInit {

  mostrarDirecto: boolean = true

  constructor(private _ref: ChangeDetectorRef, public precioBitcoinProvider: PrecioBitcoinProvider) { }

  set enVivo(valor: boolean) {

    this.mostrarDirecto = valor;

    if(valor){
      // Si desde el padre nos llega un enVivo a true
      // Queremos mostrar los cambios de precio en vivo, por lo que
      // nos aseguramos de que el TS y HTML estén acoplados
      this._ref.reattach()
    }else{
      // Si desde el padre nos llega un enVivo a false
      // Queremos congelar los cambios de precio, por lo que
      // nos aseguramos de que el TS y HTML estén desacoplados
      this._ref.detach()
    }

  }



  ngOnInit(): void {
  }

}
