import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OnReattachComponent } from './on-reattach.component';

describe('OnReattachComponent', () => {
  let component: OnReattachComponent;
  let fixture: ComponentFixture<OnReattachComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OnReattachComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OnReattachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
