import { Component, OnInit, Input, ChangeDetectionStrategy } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-async-pipe',
  templateUrl: './async-pipe.component.html',
  styleUrls: ['./async-pipe.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AsyncPipeComponent implements OnInit {

  // Observable que nos bva a ir entregando nuevas listas de items desde change-detection-page.component.ts
  @Input() items$ !: Observable<any>;

  // Lista de Items del componente
  listaItems: [] = [];

  constructor() { }

  ngOnInit(): void {

    this.items$.subscribe(
      (lista) => { this.listaItems = lista },
      (error) => console.log(`Ha ocurrido un error al obtener la lista de items: ${error}`),
      () => console.log('Nuevos items recibidos')
    );

  }

}
