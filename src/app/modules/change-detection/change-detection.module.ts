import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChangeDetectionRoutingModule } from './change-detection-routing.module';
import { ChangeDetectionPageComponent } from './pages/change-detection-page/change-detection-page.component';
import { OnReattachComponent } from './views/on-reattach/on-reattach.component';
import { OnPushComponent } from './views/on-push/on-push.component';
import { OnDetachComponent } from './views/on-detach/on-detach.component';
import { NgZoneComponent } from './views/ng-zone/ng-zone.component';
import { AsyncPipeComponent } from './views/async-pipe/async-pipe.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    ChangeDetectionPageComponent,
    OnReattachComponent,
    OnPushComponent,
    OnDetachComponent,
    NgZoneComponent,
    AsyncPipeComponent
  ],
  imports: [
    CommonModule,
    ChangeDetectionRoutingModule,
    FormsModule
  ]
})
export class ChangeDetectionModule { }
