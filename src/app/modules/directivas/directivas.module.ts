import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DirectivasRoutingModule } from './directivas-routing.module';
import { DirectivasPageComponent } from './pages/directivas-page/directivas-page.component';
import { AttrDirective } from './directives/attr.directive';
import { StructDirective } from './directives/struct.directive';
import { LifecycleDirective } from './directives/lifecycle.directive';
import { EspiadoComponent } from './components/espiado/espiado.component';


@NgModule({
  declarations: [
    DirectivasPageComponent,
    AttrDirective,
    StructDirective,
    LifecycleDirective,
    EspiadoComponent
  ],
  imports: [
    CommonModule,
    DirectivasRoutingModule
  ]
})
export class DirectivasModule { }
