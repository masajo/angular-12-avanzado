import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directivas-page',
  templateUrl: './directivas-page.component.html',
  styleUrls: ['./directivas-page.component.scss']
})
export class DirectivasPageComponent implements OnInit {

  // ? Para el ejemplo de Directiva de Atributo:
  // Definimos el color que se va a usar para el subrayado
  color: string = '';
  // ? Para el ejemplo de Directiva Estructural y Directiva LIfe Cycle:
  // Definimos el rol, en forma de booleano, que establece si se pinta un contenido o no
  rol: boolean = false;

  // ? Para el ejemplo de Directiva LifeCycle
  numero: number = 0;


  constructor() { }

  ngOnInit(): void {
  }


  // ? Para el ejemplo de Directiva Estructural
  // Método que cambia el rol de true <=> false
  cambiarRol() {
    this.rol = !this.rol;
  }

  // ? Para el ejemplo de Directiva LifeCycle
  // Método que aumenta el número y forzará cambios en el
  // componente espiado y se ejecutará su hook OnChanges().
  aumentarNumero() {
    this.numero += 1;
  }



}
