import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectivasPageComponent } from './directivas-page.component';

describe('DirectivasPageComponent', () => {
  let component: DirectivasPageComponent;
  let fixture: ComponentFixture<DirectivasPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DirectivasPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectivasPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
