import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

// Directivas estructurales que conocemos:
// * ngFor, * ngSwitch * ngCase * ngDefault, * ngIf
// Nosotros podemos crear nuestras propias directivas esctructurales
// Igual que las oficiales, se usan con un *Selector
// En nuestro caso, se usará con *rolCorrecto
// Esta directiva estructural, va a actual parecido a un ngIf, pero podríamos
// complicarlo tanto como quisiéramos.

@Directive({
  selector: '[rolCorrecto]'
})
export class StructDirective {

  // Para conocer su está mostrando o no la vista oculta vamos a usar una variable booleana
  private _mostrando: boolean = false;

  constructor(private _templateRef: TemplateRef<any>, private _viewContainerRef: ViewContainerRef) { }

  // Definimos el @Input como un Setter
  @Input() set rolCorrecto(condicion: boolean) {

    if (!condicion && !this._mostrando) {
      // En el Cotenedor del DOM al que hace referencia esta directiva
      // le indicamos que debe pintar la plantilla templateRef
      this._viewContainerRef.createEmbeddedView(this._templateRef);
      this._mostrando = true;
    } else if (condicion && this._mostrando){
      // Limpiamos le contenido del contenedor al que hace referencia la directiva
      this._viewContainerRef.clear();
      this._mostrando = false;
    }
  }
}
