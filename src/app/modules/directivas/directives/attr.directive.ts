import { Directive, ElementRef, Input, HostListener } from '@angular/core';

@Directive({
  selector: '[appHighLight]'
})
export class AttrDirective {

  @Input() defaultColor = ''
  @Input('appHighLight') highLightColor = '';

  @HostListener('mouseenter') onMouseEnter() {
    // Cuando el elemento al que hacemos referencia con esta directiva
    // recibe el cursor del ratón, le cambiamos el color
    // Llamar al métodoo que cambia el color con el color elegido, el pordefecto o rojo si noviene ninguno
    this._cambiarColor(this.highLightColor || this.defaultColor || 'red')
  }

  @HostListener('mouseleave') onMouseLeave() {
    // Cuando el elemento al que hacemos referencia con esta directiva
    // ya no tiene el cursor del ratón, le quitamos el color de fondo
    this._cambiarColor(null)
  }

  constructor(private _elementRef: ElementRef) { }

  private _cambiarColor(color: string | null){
    // Accedemos al elemento nativo de HTML al que estamos refiréndonos con esta directiva
    // y le cambiamos el style, concretamente el color de fondo con el color que recibamos por parámetro
    this._elementRef.nativeElement.style.backgroundColor = color;
  }



}
