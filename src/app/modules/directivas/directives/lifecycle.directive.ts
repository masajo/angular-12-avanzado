import { Directive, OnInit, OnDestroy } from '@angular/core';

// Directiva que ejecuta en los ciclos de vida de los componente
// que la tengan, aquello que queramos

@Directive({
  selector: '[appLifecycle]'
})
export class LifecycleDirective implements OnInit, OnDestroy {

  constructor() { }

  // Al iniciar el componente, antes de pintarlo
  ngOnInit(): void {
    this.cicloDeVida('ngOnInit');
  }

  // Cuando el componente desaparece
  ngOnDestroy(): void {
    this.cicloDeVida('ngOnDestroy');
  }

  // Método que se encarga de pintar por consola el
  // hook del ciclo de vida que está ejecutando en el componente
  cicloDeVida(hook: string) {
    console.log(`CICLO DE VIDA: ${hook}`);
  }

}
