import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DirectivasPageComponent } from './pages/directivas-page/directivas-page.component';

const routes: Routes = [
  {
    path: '',
    component: DirectivasPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DirectivasRoutingModule { }
