import { Component, Input, OnInit, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-espiado',
  templateUrl: './espiado.component.html',
  styleUrls: ['./espiado.component.scss']
})
export class EspiadoComponent implements OnInit, OnChanges {

  @Input() numero: number = 0

  constructor() { }

  ngOnInit(): void {

  }

  ngOnChanges(changes: SimpleChanges): void {
    console.log('OnChanges:', changes);
  }

}
