import { Injectable } from '@angular/core';
import { PreloadingStrategy, Route } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';

// ESTA ESTRATEGIA TIENE QUE ESTAR EN LOS PROVIDERS DEL MÓDULO QUE LA VAYA A USAR (app.module.ts)
@Injectable({ providedIn: 'root' })
export class OptInPreloadingStrategy implements PreloadingStrategy {

  // Método PRELOAD se encarga de buscar cada ruta (pasada por parámetro) y ver si tiene una propiedad
  // dentro de "data" llamada "preload" y que sea true
  // la ruta debe tener ==> {data: {preload: true}}
  // de ser true, ejecuta el callback llamado "load()" (pasado por parámetro) que cargará el módulo
  // el método load() lo ejecuta Angular para cargar el módulo
  // de no cumplir, devuelve un Observable de tipo null o EMPTY
  preload(route: Route, load: () => Observable<any> ): Observable<any> {
    return route.data && route.data['preload'] ? load() : EMPTY;
  }

}
