import { Injectable } from '@angular/core';
import { PreloadingStrategy, Route } from '@angular/router';
import { Observable, EMPTY } from 'rxjs';

// avoid typing issues for now
export declare var navigator:any;

// ESTA ESTRATEGIA TIENE QUE ESTAR EN LOS PROVIDERS DEL MÓDULO QUE LA VAYA A USAR (app.module.ts)
@Injectable({ providedIn: 'root' })
export class NetworkAwarePreloadingStrategy implements PreloadingStrategy {

  // Método que va a PRECARGAR TODOS MÓDULOS DE LAS RUTAS SIEMPRE Y CUANDO
  // HAYA BUENA CONEXIÓN
  preload(route: Route, load: () => Observable<any>): Observable<any> {
    return this.hasGoodConnection() ? load() : EMPTY;
  }

  // * MÉTODO QUE DEFINE SI HAY O NO HAY BUENA CONEXIÓN
  hasGoodConnection(): boolean {

    // Obtenemos la conexión del navegador
    const conn = navigator.connection;

    if (conn) {

      // * SI EL USUAURIO ESTÁ CON DATOS MÓVILES --> NO PRECARGAR
      if (conn.saveData) {
        return false;
      }

      // * Define una lista de conexiones A EVITAR => lentas, que no deben precargar módulos
      // para así agilizar la carga de la aplicación
      const avoidTheseConnections = ['slow-2g', '2g' /* , '3g', '4g' */];

      // * A través de conn.effectiveType obtenemos el tipo de conexión del navegador
      // nos devolverá 'slow-2g', '2g' /* , '3g' o '4g'...
      // En caso que no haya conexión, puede venir ""
      const effectiveType = conn.effectiveType || '';

      // * Si la conexión está dentro e la lista de conexiones a EVITAR
      // * Entonces no se precarga
      if (avoidTheseConnections.includes(effectiveType)) {
        return false;
      }
    }

    // Si cumple con ser una conexión buena, se precarga todo (true)
    return true;
  }
}
