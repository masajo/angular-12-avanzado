import { Injectable } from '@angular/core';
import { PreloadingStrategy, Route } from '@angular/router';
import { EMPTY, Observable, of } from 'rxjs';
import { mergeMap } from 'rxjs/operators';
import { PreloadingOptions, PreloadingService } from '../services/preloading.service';


/**
 * ESTRATEGIA DE CARGA DERUTAS PERSONALIZADA QUE SE VA A ENCARGAR DE CARGAR O NO
 * LAS DIFERENTES RUTAS QUE RECIBA EN SU MÉTODOS
 *
 * LA IDEA ES QUE A TRAVÉS DE UN EVENTO DEL USUARIO EN EL HTML
 * POR EJEMPLO, UN CLICK EN UN BOTÓN O UN HOVER SOBRE UNA OPCIÓN DE
 * UN MENÚ, CARGUE UN MÓDULO DE RUTAS CONCRETAS.
 *
 * ESTA ESTRATEGIA SIRVE PARA ADELANTARSE AL COMPORTAMIENTO DEL USUARIO
 * Y CARGAR DE FORMA DINÁMICA/PRECARGAR ANTES DE QUE EL USUARIO SIQUIERA
 * HAYA PULSADO O ACCEDIDO A LA RUTA.
 *
 * DE ESTA MANERA, LOS TIEMPOS DE ESPERA DEL USUARIO SE REDUCEN EN LA
 * EXPERIENCIA DE NAVEGACIÓN.
 *
 * POR EJEMPLO:
 * UN USUARIO PASA POR ENCIMA DE UNA OPCIÓN DEL MENÚ Y NOSOTROS
 * PODEMOS CARGAR ESE MÓDULO, ANTES DE QUE EL USUARIO LO HAYA PULSADO.
 *
 */

@Injectable({
  providedIn: 'root',
  // dependencias de esta clase
  deps: [
    PreloadingService
  ]
})
export class OnDemandPreloadingStrategy implements PreloadingStrategy {

  // Definimos el Observable (Subject) que obtendermos de nuestro servicio PreloadingService
  // con las opciones que tiene la ruta que se quiere cargar desde un botón
  private _preloadOnDemandOptions$: Observable<PreloadingOptions>;

  constructor(private _preloadingService: PreloadingService) {
    // Inicializamos a partir del Observable del Servicio
    this._preloadOnDemandOptions$ = this._preloadingService.options$
  }

  // Método para saber si PRECARGAR O NO PRECARGAR UNA RUTA SOLICITADA
  private decidirSiCargar(route: Route, preloadOptions: PreloadingOptions): boolean {
    // Si todas estas condiciones se cumplen, la ruta deberá cargarse
    return (
      route.data &&
      route.data['preload'] &&
      [route.path, '*'].includes(preloadOptions.routePath) &&
      preloadOptions.preload // true
    )
  }

  // El método PRELOAD esta vez es MÁS COMPLEJO ya que usará el método decidirSiCargar
  // para comprobar si una ruta debe ser precargada o no
  preload(route: Route, load: () => Observable<any>): Observable<any> {

    return this._preloadOnDemandOptions$.pipe(
      mergeMap((preloadOptions) => {
        // Comprobamos si se debe o no precargar
        const precarga = this.decidirSiCargar(route, preloadOptions);
        // Mostramos por consola si precargamos o no una ruta
        console.log(`${precarga ? '' : 'NO'} precargamos la ruta ${route.path}`);
        // Ejecutamos "load()" únicamente si "precarga" es true
        return precarga ? load() : EMPTY
      })
    );
  }

}
