import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { PreloadingService } from 'src/app/services/preloading.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver, private _preloadingService: PreloadingService) {}

  // MÉTODOS PARA PRECARGAR RUTAS DESDE LOS BOTONES DEL HTML, sin necesidad de navegar a esas rutas
  cargarTodasLasRutas() {
    this._preloadingService.startPreload('*');
  }

  // Cargar una ruta espcífica que tendrá que pasar el control
  // de decidirSiCargar de la estrategia de OnDemandPreloadingStrategy
  // Es decir, si la ruta no preload:true y otras condiciones, no cargará
  cargarRuta(ruta: string) {
    this._preloadingService.startPreload(ruta);
  }



}
