import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';


// Vamos a crear una clase que nos va a servir para definir las opciones
// que debe tener una ruta para ser cargada
export class PreloadingOptions {
  constructor(public routePath:string, public preload: boolean = true){}
}


/**
 * SERVICIO PERSONALIZADO QUE SE VA A ENCARGAR DE CARGAR O NO
 * LAS DIFERENTES RUTAS QUE RECIBA EN SU MÉTODOS
 *
 * LA IDEA ES QUE A TRAVÉS DE UN EVENTO DEL USUARIO EN EL HTML
 * POR EJEMPLO, UN CLICK EN UN BOTÓN O UN HOVER SOBRE UNA OPCIÓN DE
 * UN MENÚ, CARGUE UN MÓDULO DE RUTAS CONCRETAS.
 *
 * ESTA ESTRATEGIA SIRVE PARA ADELANTARSE AL COMPORTAMIENTO DEL USUARIO
 * Y CARGAR DE FORMA DINÁMICA/PRECARGAR ANTES DE QUE EL USUARIO SIQUIERA
 * HAYA PULSADO O ACCEDIDO A LA RUTA.
 *
 * DE ESTA MANERA, LOS TIEMPOS DE ESPERA DEL USUARIO SE REDUCEN EN LA
 * EXPERIENCIA DE NAVEGACIÓN.
 *
 * POR EJEMPLO:
 * UN USUARIO PASA POR ENCIMA DE UNA OPCIÓN DEL MENÚ Y NOSOTROS
 * PODEMOS CARGAR ESE MÓDULO, ANTES DE QUE EL USUARIO LO HAYA PULSADO.
 *
 */

@Injectable({
  providedIn: 'root'
})
export class PreloadingService {

  // Un Subject es un tipo de Observable que puede emitir
  // valores a quienes estén suscritos a través del método
  // .next(nuevoValor)
  private _subject = new Subject<PreloadingOptions>();

  // * Cualquier Subject es un Observable y es el que tenemos que hacer público
  // * Vamos a ofrecer las opciones de la ruta como un Observable
  public options$ = this._subject.asObservable()

  constructor() { }

  // * Método que vamos a usar desde los COMPONENTE para empezar
  // * a cargar la ruta que le pasemos por parámetro
  // POR EJEMPLO: DESDE UN BOTÓN SOLICITAMOS CARGAR LA RUTA, "/PROFILE"
  // El servicio, emite esa ptición a la estrategia ONDEMAND
  startPreload(routePath: string) {
    const options = new PreloadingOptions(routePath, true);
    // Emitimos las opciones de la ruta que queremos empezar a cargar A LA ESTRATEGIA
    // QUE VA A ESTAR ESCUCHANDO RUTAS QUE DESAN SER PRECARGADAS
    this._subject.next(options);
  }

}
